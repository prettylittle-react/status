import React from 'react';
import PropTypes from 'prop-types';

import {Content, Editable, Heading, Img, RichText, Text} from 'editable';

import './Status.scss';

/**
 * Status
 * @description [Description]
 * @example
  <div id="Status"></div>
  <script>
    ReactDOM.render(React.createElement(Components.Status, {
    	title : 'Example Status'
    }), document.getElementById("Status"));
  </script>
 */
class Status extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'status';
	}

	render() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				<Text content="New" className={`${this.baseClass}__label`} />
				<Text content="Currently available for new projects" />
			</div>
		);
	}
}

Status.defaultProps = {
	children: null
};

Status.propTypes = {
	children: PropTypes.node
};

export default Status;
